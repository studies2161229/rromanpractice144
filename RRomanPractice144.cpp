

//I add empty lines because it's easier on my eyes. Perhaps this is bad


#include <iostream>
#include <string>

int main()
{
    std::string sRoma = "Greetings, I am Roma!";

    std::cout << " \n";
    std::cout << sRoma << "\n";


    std::cout << sRoma.length() << "\n";
    std::cout << sRoma.front() << "\n";
    std::cout << sRoma.back() << "\n" << "\n";


//This does the same thing as above, but with your custom string

    std::cout << "Enter some text please ^_^\n";
    std::string InputString;
    std::getline(std::cin, InputString);

    std::cout << InputString.length() << "\n";

    std::cout << InputString.front() << "\n";

    std::cout << InputString.back() << "\n";
}
